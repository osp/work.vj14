Are You Being Served?
=====================

Web and print publication of the festival Verbindingen/Jonctions run by Constant in December 2013.
Made with Ethertoff.

Avant le projet *f-u-t-u-r-e.org*,  la première version d'Ethertoff a été utilisé pour les prises de note collectives du festival Verbindingen/Jonctions 14. Après le lancement de *f-u-t-u-r-e.org*, lors de l'édition des prises de notes, il était logique de profiter des avancements du logiciel pour produire la publication imprimée et web.

Dans ce projet, nous avons tenté de sortir du flux de mise en page très strict imposé par la structure d'Ethertoff en ayant des métadonnées et notes de bas de page dans les marges, ainsi que des séries d'images qui prennent toute la largeur de la page.


The design of this book has been made with HTML, (less)CSS and Javascript on the platform Ethertoff <http://osp.kitchen/tools/ethertoff/>. The layout has been inspired by the cookbook Recette des Provinces de France by the food critique Curnonsky, published by Les Productions de Paris & P.E. Lamaison in 1962. Here is the incipit of the book: “Voici un ouvrage d’une conception originale et nouvelle, et d’une présentation parfaite.”


Cover illustration taken from Elizabeth Raffald, The Experienced English Housekeeper (Manchester: Printed by J. Harrep), 1769.
Featured fonts: Domine, Quattrocento Sans, Prop Courier Sans, Young Serif with a Windsoresque “a”.




![](http://git.constantvzw.org/?p=osp.tools.ethertoff.git;a=blob_plain;f=iceberg/vj14_spread-in-browser.png;hb=HEAD)
:    Vue en double-pages dans Ethertoff
    
![](http://git.constantvzw.org/?p=osp.tools.ethertoff.git;a=blob_plain;f=iceberg/vj14_chemin-de-fer-in-browser.png;hb=HEAD)
:    Vue en chemin de fer dans Ethertoff
    
    
![](http://git.constantvzw.org/?p=osp.tools.ethertoff.git;a=blob_plain;f=iceberg/vj14_pdf.png;hb=HEAD)
:    Export PDF



![](iceberg/DSC_9959.png)
:    Cover flattened: the tags in the plates are randomly picked, as we used digital printing, each cover is different

![](iceberg/DSC_9957.png)
:    Spread: margins are are used for metadata and footnotes. Gallery images uses the whole width of the page to win space but mostly to break with the strict flow layout imposed by the structure of Ethertoff.


![](iceberg/DSC_9958.png)
:    The format and the design were inspired by a cookbook of the French food critique Curnonsky.
